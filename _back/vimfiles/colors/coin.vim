" Name: coin
" Author: ZeDuckMaster
" Version: 0.1
" URL: 
" License: GPL

highlight clear
set background=dark
if exists("syntax_on")
    syntax reset
endif
let g:colors_name = "coin"

"default
highlight Normal		guifg=#DCDCDC	guibg=#1E1E1E	gui=none
highlight Visual		guifg=NONE		guibg=#0E4583	gui=none
highlight Comment		guifg=#525252	guibg=NONE		gui=none
highlight Constant		guifg=#BD63C5	guibg=NONE		gui=none
highlight Number		guifg=#E87171	guibg=NONE		gui=none
highlight String		guifg=#D69D85	guibg=NONE		gui=none
highlight Directory		guifg=#4EC9B0	guibg=NONE		gui=none
highlight Special		guifg=#D69D85	guibg=NONE		gui=none
highlight LineNr		guifg=#2B91AF	guibg=NONE		gui=none
highlight CursorLine	guifg=NONE		guibg=#0F0F0F	gui=none
highlight CursorLineNr	guifg=#FFFFFF	guibg=#0F0F0F	gui=none
highlight VertSplit		guifg=#4A4A4A	guibg=#4A4A4A	gui=none
highlight StatusLine	guifg=#000000	guibg=#9E9E9E	gui=none
highlight StatusLineNC	guifg=#B2B2B2	guibg=#4A4A4A	gui=none
"delimiters and operators
highlight Operator		guifg=#00FFFF	guibg=NONE		gui=none
highlight Delimiter		guifg=#00FFFF	guibg=NONE		gui=none
"language
highlight Identifier	guifg=#4EC9B0	guibg=NONE		gui=none
highlight Type			guifg=#4EC9B0	guibg=NONE		gui=none
highlight Error			guifg=#FF0000	guibg=NONE		gui=none
highlight Statement		guifg=#569CD6	guibg=NONE		gui=none
highlight Keyword		guifg=#569CD6	guibg=NONE		gui=none
highlight PreProc		guifg=#9B9B9B	guibg=NONE		gui=none
highlight Macro			guifg=#9B9B9B	guibg=NONE		gui=none

