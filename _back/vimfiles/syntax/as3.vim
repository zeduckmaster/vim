" Vim syntax file
" Language: AS3
" Maintainer: ZeDuckMaster
" Latest Revision:

"if !exists("main_syntax")
"	if exists("b:current_syntax")
"		finish
"	endif
	let main_syntax = 'as3'
"endif

syn match as3LineComment	"\/\/.*$"
syn match as3Number			"-\=\<\d\+L\=\>\|0[xX][0-9a-fA-F]\+\>"
syn match as3Operators		"[](){}<>=+*/[]"

syn keyword as3Statements	break case continue default do while else for in each if label return super switch throw try catch finally while with is as new
syn keyword as3Keyword		void var class function import package dynamic final internal native override private protected public static

syn keyword as3Constants	true false null

syn keyword as3Base			ArgumentError Array Boolean Class Date DefinitionError Error EvalError Function int JSON  Math Namespace Number Object QName RangeError ReferenceError RegExp SecurityError String SyntaxError TypeError uint URIError Vector VerifyError XML XMLList
syn keyword as3FlashEvents	AccelerometerEvent ActivityEvent AsyncErrorEvent AVDictionaryDataEvent Event EventDispatcher 

hi def link as3LineComment	Comment
hi def link as3Number		Number
hi def link as3Operators	Operator
hi def link as3Statements	Statement
hi def link as3Keyword		Keyword
hi def link as3Constants	Constant
hi def link as3Base			Identifier
hi def link as3FlashEvents	Identifier

let b:current_syntax = "as3"
if main_syntax == 'as3'
	unlet main_syntax
endif
