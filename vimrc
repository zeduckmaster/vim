set mouse=a
set backspace=2
set autochdir
cd $HOME

set t_Co=256
set guifont=Consolas:h12
colors kolor
syntax on

set tabstop=4
set softtabstop=0
set noexpandtab
set shiftwidth=4
set cursorline
set number

" netrw config
:let g:netrw_list_hide= '.*\.swp$,\~$'
